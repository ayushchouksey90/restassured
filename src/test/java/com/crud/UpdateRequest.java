package com.crud;

import static io.restassured.RestAssured.*;

import com.pojo.CrudRunner;

import io.restassured.http.Header;
import io.restassured.response.Response;

public class UpdateRequest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		baseURI = "http://localhost:3000";
		Response response = given().
				when().header(new Header("content-type","application/json")).
				and().body(new CrudRunner ("Ayush Chouksey","ayushchouksey29@gmail.com","35/4 pushpa electricals pardeshipura").toJson()).put("/profile");
		System.out.println(response.asPrettyString());
	}

}
