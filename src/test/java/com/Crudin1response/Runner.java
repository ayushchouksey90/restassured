package com.Crudin1response;

import java.io.IOException;

import com.pojo.CrudRunner;

import io.restassured.http.Header;

public class Runner {
	
	
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
	
		APIHelper helper = new APIHelper();
		
		helper.makeRequest(Verb.Post, "/posts",new CrudRunner("ayush", "ayushchouksey90@gmail.com", "35/4 pushpa electricals pardeshipura"), 
				new Header("Content-Type","application/json")).then().log().all();
		helper.makeRequest(Verb.Get, "/posts",new CrudRunner("ayush", "ayushchouksey90@gmail.com", "35/4 pushpa electricals pardeshipura"), 
				new Header("Content-Type","application/json")).then().log().all();
		helper.makeRequest(Verb.Put, "/posts",new CrudRunner("Ayush Chouksey", "ayushchouksey29@gmail.com", "35/4 pushpa electricals pardeshipura"), 
				new Header("Content-Type","application/json")).then().log().all();
		helper.makeRequest(Verb.Delete, "/posts",new CrudRunner("ayush", "ayushchouksey90@gmail.com", "35/4 pushpa electricals pardeshipura"), 
				new Header("Content-Type","application/json")).then().log().all();
		
	}

}
