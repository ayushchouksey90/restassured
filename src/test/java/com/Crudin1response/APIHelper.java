package com.Crudin1response;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.pojo.IBody;

import static io.restassured.RestAssured.*;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.response.Response;

public class APIHelper {

	List<Header> myheaderList = new ArrayList<Header>();
	IBody body;
		
		
		public Response makeRequest(Verb httpVerb, String endPoint, IBody body, Header... headerList) throws IOException {
			
			baseURI = "https://localhost:3000";
			Response response = null;
			for(Header h : headerList) {
				myheaderList.add(h);
			}
			
			if (Verb.Get == httpVerb) {
				response = given().headers(new Headers(headerList)).and().body(body.toJson()).when().get(endPoint);
			}else if(Verb.Post == httpVerb) {
				response = given().headers(new Headers(headerList)).and().body(body.toJson()).when().get(endPoint);
			}else if(Verb.Put == httpVerb) {
				response = given().headers(new Headers(headerList)).and().body(body.toJson()).when().get(endPoint);
			}else if(Verb.Delete == httpVerb) {
				response = given().headers(new Headers(headerList)).and().body(body.toJson()).when().get(endPoint);
			}
				return response;
		
		}
		
		}
		
		
