package com.pojo;

import com.google.gson.Gson;

public class CrudRunner implements IBody {
	
	private String name;
	private String email;
	private String address;
	public CrudRunner(String name, String email, String address) {
		super();
		this.name = name;
		this.email = email;
		this.address = address;
	}
	@Override
	public String toString() {
		return "Runnerday3 [name=" + name + ", email=" + email + ", address=" + address + "]";
	}
	
	public String toJson() {
		CrudRunner b = new CrudRunner(name,email,address);
		Gson g = new Gson();
		String data = g.toJson(b);
		return data;
		
	}

}
