package com.pojo;

import com.google.gson.Gson;

public class Runnerday2 {
	
	private String name;
	private String job;
	public Runnerday2(String name, String job) {
		super();
		this.name = name;
		this.job = job;
	}
	@Override
	public String toString() {
		return "day02 [name=" + name + ", job=" + job + "]";
	}
	
	public String toJson() {
		Runnerday2 a = new Runnerday2 (name,job);
		Gson g = new Gson();
		String data = g.toJson(a);
		return data;
		
	}
	
}
