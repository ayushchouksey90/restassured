package com.assertions;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.hamcrest.Matchers;

import com.pojo.CrudRunner;

import io.restassured.http.Header;

public class assertions {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		baseURI = "http://localhost:3000";
		given().header(new Header("content-type","application/json")).
				body(new CrudRunner ("ayush","ayushchouksey90@gmail.com","35/4 pushpa electricals pardeshipura").toJson()).when().post("/posts").
				then().assertThat().statusCode(201).and().log().all().and().body("status", Matchers.equalTo("Created"));
	}

}
