package com.utils;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class Testutils {
	public static String readConfigfile(String fileName, String key) throws IOException{
		
		File myfile = new File("C:\\Users\\LENOVO\\eclipse-workspace\\Restassured\\config\\" + fileName);
		FileReader fr = new FileReader(myfile);
		
		Properties prop = new Properties();
		prop.load(fr);
		String data = prop.getProperty(key);
		return data;
	}

}
