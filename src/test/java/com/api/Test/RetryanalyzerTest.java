package com.api.Test;

import java.io.IOException;

import org.testng.annotations.Test;

import com.Crudin1response.APIHelper;
import com.Crudin1response.Verb;
import com.pojo.CrudRunner;

import io.restassured.http.Header;

public class RetryanalyzerTest {
	
	APIHelper helper = new APIHelper();
	
	@Test(groups = {"api","smoke","sanity","e2e"},retryAnalyzer = com.listners.myretryanalyzer.class)
	public void verifyAPIrequest() throws IOException {
		
		helper.makeRequest(Verb.Post, "/posts",new CrudRunner("ayush", "ayushchouksey90@gmail.com", "35/4 pushpa electricals pardeshipura"), 
				new Header("Content-Type","application/json")).then().log().all().assertThat().and().statusCode(200);
	}
}
