package Testrest;

import static io.restassured.RestAssured.*;

import com.pojo.Runnerday2;

import io.restassured.http.Header;
import io.restassured.response.Response;

public class day2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		baseURI = ("https://reqres.in");
		Response response = given().
				when().header(new Header("content-type","application/json")).
				and().body(new Runnerday2 ("john","leader").toJson()).post("/api/users");
		System.out.println(response.asPrettyString());
	}

}
