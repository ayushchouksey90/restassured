package Testrest;

import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class testing {
	public static void main(String[]args) {
		
		RestAssured.baseURI = "https://reqres.in";
		RequestSpecification myrequest = RestAssured.given();
		Response response = myrequest.post();
		Header myHeader = new Header("Content-Type", "application/json");
		myrequest.header(myHeader);
		myrequest.body("{\r\n"
				+ "    \"name\" : \"john\",\r\n"
				+ "    \"job\"  : \"leader\"\r\n"
				+ "}");
		Response response1 = myrequest.post("/api/users");
		System.out.println(response1.asPrettyString());
	}

}
