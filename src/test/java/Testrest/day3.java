package Testrest;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import java.io.IOException;
import com.utils.Testutils;

import io.restassured.http.Header;
import io.restassured.response.Response;

public class day3 {

	public static void main(String[] args)throws IOException {
		// TODO Auto-generated method stub
		baseURI = Testutils.readConfigfile("read.properties", "URL");
		Response response = given().
				when().header(new Header("content-type","application/json")).
				and().body("{\r\n"
						+ "    \"name\" : \"ayush \",\r\n"
						+ "    \"email\"  : \"ayushchouksey90@gmail.com\",\r\n"
						+ "    \"address\" : \"35/4 pushpa electricals pardeshipura\"\r\n"+"}").post("/posts");
		System.out.println(response.asPrettyString());
	}

}
